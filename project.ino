// INF-746
// Gustavo F T Pedrosa

#include <math.h>
#include <Servo.h>
#include "thermistor.h"
#include <Wire.h>
#include <TH02_dev.h>
#include "rgb_lcd.h"
#include "ArduinoJson/ArduinoJson.h"

#include <stdio.h>
#include <stdlib.h>
#include <ctype.h>
#include <unistd.h>
#include <string.h>
#include <limits.h>

#include "jsmn.h"
#include "mbedtls-wrapper.h"

#include "aws_iot_config.h"
#include "aws_iot_log.h"
#include "aws_iot_version.h"
#include "aws_iot_mqtt_client_interface.h"
#include "aws_iot_shadow_interface.h"

#define DEBUG_VAR(X)    \
    Serial.print(#X);   \
    Serial.print(": "); \
    Serial.println(X);

#undef AWS_IOT_MQTT_HOST
#undef AWS_IOT_MQTT_PORT
#undef AWS_IOT_MQTT_CLIENT_ID
#undef AWS_IOT_MY_THING_NAME
#undef AWS_IOT_ROOT_CA_FILENAME
#undef AWS_IOT_CERTIFICATE_FILENAME
#undef AWS_IOT_PRIVATE_KEY_FILENAME
#undef AWS_IOT_MQTT_RX_BUF_LEN

#define AWS_IOT_MQTT_HOST "a3niycmyotlgq1.iot.us-east-1.amazonaws.com"
#define AWS_IOT_MQTT_PORT 8883
#define AWS_IOT_MQTT_CLIENT_ID "gustavo"
#define AWS_IOT_MY_THING_NAME "project"
#define AWS_IOT_ROOT_CA_FILENAME \
    "VeriSign-Class-3-Public-Primary-Certification-Authority-G5.pem"
#define AWS_IOT_CERTIFICATE_FILENAME "dc77791bb6-certificate.pem.crt"
#define AWS_IOT_PRIVATE_KEY_FILENAME "dc77791bb6-private.pem.key"
#define AWS_IOT_CERTS "/opt/certs"
#define MAX_LENGTH_OF_UPDATE_JSON_BUFFER 250
#define AWS_IOT_MQTT_RX_BUF_LEN 1024
/* https://forums.aws.amazon.com/thread.jspa?threadID=219755 */

// Analog pin used to read the NTC
#define NTC_PIN               A3

// Thermistor object
THERMISTOR thermistor(NTC_PIN,        // Analog pin
                      10000,          // Nominal resistance at 25 ?C
                      3950,           // thermistor's beta coefficient
                      10000);         // Value of the series resistor

rgb_lcd lcd;
Servo myservo;
const int pinRelay = 5;
const int pinServo = 6;
const int buttonPin = 0;
const int pinMoisture = A1;
char OFF = 'O';
char GAS = 'G';
char ELE = 'E';

volatile unsigned int local_button = 0;
unsigned int remote_button = 0;
char status_state[2]; //O, G, or E

float gas_cost = 0.8;
float electric_cost = 1.2;
float target_temperature = 29.1;
float water_temperature = 0.0;
float moisture_level = 0.0;
float efficiency = 0.0;

static char certDirectory[] = AWS_IOT_CERTS;
StaticJsonBuffer<AWS_IOT_MQTT_RX_BUF_LEN> jsonBuffer;
String jsonBufferStr;
AWS_IoT_Client mqttClient;

unsigned long previousMillis = 0;
unsigned long currentMillis = 0;
const unsigned int refreshRate = 3000;

jsonStruct_t remoteButton;
jsonStruct_t gasCost;
jsonStruct_t electricCost;
jsonStruct_t targetTemperature;
jsonStruct_t waterTemperature;
jsonStruct_t statusState;

/* Update Shadow Gas Cost */
void updateShadowGasCost()
{
    /* Initialization */
    IoT_Error_t rc = FAILURE;
    char JsonDocumentBuffer[MAX_LENGTH_OF_UPDATE_JSON_BUFFER];
    size_t sizeOfJsonDocumentBuffer = sizeof(JsonDocumentBuffer) / sizeof(JsonDocumentBuffer[0]);

    /* open json document */
    rc = aws_iot_shadow_init_json_document(JsonDocumentBuffer,
        sizeOfJsonDocumentBuffer);
    if (SUCCESS == rc) {
        rc = aws_iot_shadow_add_reported(JsonDocumentBuffer,
            sizeOfJsonDocumentBuffer, 1, &gasCost); // add gas_cost reported
        if (SUCCESS == rc) {
            rc = aws_iot_finalize_json_document(JsonDocumentBuffer,
                sizeOfJsonDocumentBuffer); // close json document
            if (SUCCESS == rc) {
                IOT_INFO("Update Shadow: %s", JsonDocumentBuffer);
                Serial.print("Update Shadow: ");
                Serial.println(JsonDocumentBuffer);
                rc = aws_iot_shadow_update(
                    &mqttClient, AWS_IOT_MY_THING_NAME, JsonDocumentBuffer,
                    ShadowUpdateStatusCallback, NULL, 4, true); // 4 seconds
            }
        }
    }
}

/* Update Shadow Electric Cost */
void updateShadowElectricCost()
{
    /* Initialization */
    IoT_Error_t rc = FAILURE;
    char JsonDocumentBuffer[MAX_LENGTH_OF_UPDATE_JSON_BUFFER];
    size_t sizeOfJsonDocumentBuffer = sizeof(JsonDocumentBuffer) / sizeof(JsonDocumentBuffer[0]);

    /* open json document */
    rc = aws_iot_shadow_init_json_document(JsonDocumentBuffer,
        sizeOfJsonDocumentBuffer);
    if (SUCCESS == rc) {
        rc = aws_iot_shadow_add_reported( // add electric_cost reported
            JsonDocumentBuffer, sizeOfJsonDocumentBuffer, 1, &electricCost);
        if (SUCCESS == rc) {
            rc = aws_iot_finalize_json_document(JsonDocumentBuffer,
                sizeOfJsonDocumentBuffer); // close json document
            if (SUCCESS == rc) {
                IOT_INFO("Update Shadow: %s", JsonDocumentBuffer);
                Serial.print("Update Shadow: ");
                Serial.println(JsonDocumentBuffer);
                rc = aws_iot_shadow_update(
                    &mqttClient, AWS_IOT_MY_THING_NAME, JsonDocumentBuffer,
                    ShadowUpdateStatusCallback, NULL, 4, true); // 4 seconds
            }
        }
    }
}

/* Update Shadow Target Temperature */
void updateShadowTargetTemperature()
{
    /* Initialization */
    IoT_Error_t rc = FAILURE;
    char JsonDocumentBuffer[MAX_LENGTH_OF_UPDATE_JSON_BUFFER];
    size_t sizeOfJsonDocumentBuffer = sizeof(JsonDocumentBuffer) / sizeof(JsonDocumentBuffer[0]);

    /* open json document */
    rc = aws_iot_shadow_init_json_document(JsonDocumentBuffer,
        sizeOfJsonDocumentBuffer);
    if (SUCCESS == rc) {
        rc = aws_iot_shadow_add_reported( // add target_temperature reported
            JsonDocumentBuffer, sizeOfJsonDocumentBuffer, 1, &targetTemperature);
        if (SUCCESS == rc) {
            rc = aws_iot_finalize_json_document(JsonDocumentBuffer,
                sizeOfJsonDocumentBuffer); // close json document
            if (SUCCESS == rc) {
                IOT_INFO("Update Shadow: %s", JsonDocumentBuffer);
                Serial.print("Update Shadow: ");
                Serial.println(JsonDocumentBuffer);
                rc = aws_iot_shadow_update(
                    &mqttClient, AWS_IOT_MY_THING_NAME, JsonDocumentBuffer,
                    ShadowUpdateStatusCallback, NULL, 4, true); // 4 seconds
            }
        }
    }
}

/* Callback of the function: aws_iot_shadow_update */
void ShadowUpdateStatusCallback(const char* pThingName, ShadowActions_t action,
    Shadow_Ack_Status_t status,
    const char* pReceivedJsonDocument,
    void* pContextData)
{
    IOT_UNUSED(pThingName);
    IOT_UNUSED(action);
    IOT_UNUSED(pReceivedJsonDocument);
    IOT_UNUSED(pContextData);

    if (SHADOW_ACK_TIMEOUT == status) {
        IOT_INFO("Update Timeout--");
        Serial.println("Update Timeout--");
    }
    else if (SHADOW_ACK_REJECTED == status) {
        IOT_INFO("Update RejectedXX");
        Serial.println("Update RejectedXX");
    }
    else if (SHADOW_ACK_ACCEPTED == status) {
        IOT_INFO("Update Accepted !!");
        Serial.println("Update Accepted !!");
    }
}

/* Callback of the "desired" remote_button value, e.g.
{
 "state": {
  "desired": {
   "R": true
  }
 }
}

or

{
 "state": {
  "desired": {
   "R": false
  }
 }
}
 */
void remoteButton_Callback(const char* pJsonString, uint32_t JsonStringDataLen,
    jsonStruct_t* pContext)
{
    IOT_UNUSED(pJsonString);
    IOT_UNUSED(JsonStringDataLen);

    if (pContext != NULL) {
        IOT_INFO("Delta - remote_button state changed to %d",
            *(bool*)(pContext->pData));
        Serial.print("Delta - remote_button state changed to: ");
        remoteButtonPressed(*(bool*)(pContext->pData));
        Serial.println(remote_button);
    }
}

/* Callback of the "desired" gas_cost value */
void gasCost_Callback(const char* pJsonString, uint32_t JsonStringDataLen,
    jsonStruct_t* pContext)
{
    IOT_UNUSED(pJsonString);
    IOT_UNUSED(JsonStringDataLen);

    if (pContext != NULL) {
        IOT_INFO("Delta - gas_cost state changed to %d",
            *(float*)(pContext->pData));
        Serial.print("Delta - gas_cost state changed to: ");
        gas_cost = *(float*)(pContext->pData);
        Serial.println(gas_cost);

        updateShadowGasCost();
    }
}

/* Callback of the "desired" electric_cost value */
void electricCost_Callback(const char* pJsonString, uint32_t JsonStringDataLen,
    jsonStruct_t* pContext)
{
    IOT_UNUSED(pJsonString);
    IOT_UNUSED(JsonStringDataLen);

    if (pContext != NULL) {
        IOT_INFO("Delta - electric_cost state changed to %d",
            *(float*)(pContext->pData));
        Serial.print("Delta - electric_cost state changed to: ");
        electric_cost = *(float*)(pContext->pData);
        Serial.println(electric_cost);

        updateShadowElectricCost();
    }
}

/* Callback of the "desired" target_temperature value */
void targetTemperature_Callback(const char* pJsonString, uint32_t JsonStringDataLen,
    jsonStruct_t* pContext)
{
    IOT_UNUSED(pJsonString);
    IOT_UNUSED(JsonStringDataLen);

    if (pContext != NULL) {
        IOT_INFO("Delta - target_temperature state changed to %d",
            *(float*)(pContext->pData));
        Serial.print("Delta - target_temperature state changed to: ");
        target_temperature = *(float*)(pContext->pData);
        Serial.println(target_temperature);

        updateShadowTargetTemperature();
    }
}

/* Callback just for debug/logging */
void DeltaCallback(const char* pJsonValueBuffer, uint32_t valueLength,
    jsonStruct_t* pJsonStruct_t)
{
    IOT_UNUSED(pJsonStruct_t);
    IOT_DEBUG("Received Delta message %.*s", valueLength, pJsonValueBuffer);
    Serial.print("Received Delta message: ");
    Serial.println(pJsonValueBuffer);
}

/* GET callback */
void shadowCallback(const char* pThingName, ShadowActions_t action,
    Shadow_Ack_Status_t status_t, const char* pReceivedJsonDocument,
    void* pContextData)
{
    jsonBufferStr = String(pReceivedJsonDocument);
    int str_len = jsonBufferStr.length() + 1;
    char char_array[str_len];
    jsonBufferStr.toCharArray(char_array, str_len);

    JsonObject& root = jsonBuffer.parseObject(char_array);

    if (!root.success()) {
        Serial.println("parseObject() failed");
        return;
    }

    const char* s = root["state"]["reported"]["S"];
    String ss = String(s);

    if (ss.length() == 0) {
        IoT_Error_t rc = FAILURE;
        char JsonDocumentBuffer[MAX_LENGTH_OF_UPDATE_JSON_BUFFER];
        size_t sizeOfJsonDocumentBuffer = sizeof(JsonDocumentBuffer) / sizeof(JsonDocumentBuffer[0]);

        /* open json document */
        rc = aws_iot_shadow_init_json_document(JsonDocumentBuffer,
            sizeOfJsonDocumentBuffer);
        if (SUCCESS == rc) {
            rc = aws_iot_shadow_add_reported( // add 4 reporteds
                JsonDocumentBuffer, sizeOfJsonDocumentBuffer, 4, &gasCost, &electricCost,
                &targetTemperature, &remoteButton);
            if (SUCCESS == rc) {
                rc = aws_iot_finalize_json_document(JsonDocumentBuffer,
                    sizeOfJsonDocumentBuffer); // close json document
                if (SUCCESS == rc) {
                    IOT_INFO("Update Shadow: %s", JsonDocumentBuffer);
                    Serial.print("Update Shadow: ");
                    Serial.println(JsonDocumentBuffer);
                    rc = aws_iot_shadow_update(&mqttClient, AWS_IOT_MY_THING_NAME,
                        JsonDocumentBuffer,
                        ShadowUpdateStatusCallback, NULL, 4, true); // 4 seconds
                }
            }
        }
        return;
    }

    remote_button = root["state"]["reported"]["R"];
    water_temperature = root["state"]["reported"]["W"];
    gas_cost = root["state"]["reported"]["G"];
    electric_cost = root["state"]["reported"]["E"];
    target_temperature = root["state"]["reported"]["T"];
    status_state[0] = s[0];
    status_state[1] = '\0';

    Serial.println(remote_button);
    Serial.println(water_temperature);
    Serial.println(gas_cost);
    Serial.println(electric_cost);
    Serial.println(target_temperature);
    Serial.println(status_state);
}

/* Remote button cloud callback */
void remoteButtonPressed(bool value)
{
    remote_button = value;

    if (!remote_button && !local_button) {
        status_state[0] = OFF;
        setServo();
        relayHandle();
    }
}

/* Local button interrupt callback */
void localButtonPressed()
{
    local_button = !local_button;

    if (!local_button && !remote_button) {
        status_state[0] = OFF;
        setServo();
        relayHandle();
    }
}

/* Write the value position in the servo */
void servoHandle(int value)
{
    myservo.write(value);
    delay(15); // waits 15ms for the servo to reach the position
}

/* Turn relay ON or OFF */
void relayHandle()
{
    if (status_state[0] == OFF) {
        digitalWrite(pinRelay, LOW); // OFF
    }
    else {
        digitalWrite(pinRelay, HIGH); // ON
    }
}

/* Set servo position */
void setServo()
{
    if (status_state[0] == ELE) {
        servoHandle(0);
    }
    else if (status_state[0] == GAS) {
        servoHandle(180);
    }
    else {
        servoHandle(90); // Middle
    }
}

/* State machine function */
char stateMachine()
{
    if (water_temperature < target_temperature) {
        if (efficiency < electric_cost) {
            return GAS;
        }
        else {
            return ELE;
        }
    }
    else {
        return OFF;
    }
}

/* Print LCD display */
void printLCD()
{
    lcd.clear();

    lcd.print(status_state);
    lcd.print("   ");
    lcd.print(water_temperature);
    lcd.print(" C");
    lcd.setCursor(0, 1); // jump line
    lcd.print("    ");
    if (target_temperature < 9.0) {
        lcd.print(" "); // Align
    }
    lcd.print(target_temperature);
    lcd.print(" C");
}

/* Apply command */
void apply_command(char command, char value[])
{
    DEBUG_VAR(command);

    switch (tolower(command)) {
    case 'g':
        gas_cost = atof(value);
        DEBUG_VAR(gas_cost);
        updateShadowGasCost();
        break;
    case 'e':
        electric_cost = atof(value);
        DEBUG_VAR(electric_cost);
        updateShadowElectricCost();
        break;
    case 't':
        target_temperature = atof(value);
        DEBUG_VAR(target_temperature);
        updateShadowTargetTemperature();
        break;
    case 'r':
        remoteButtonPressed(atoi(value));
        DEBUG_VAR(remote_button);
        break;
    case 'l':
        local_button = atoi(value);
        DEBUG_VAR(local_button);
        break;
    case 'w':
        water_temperature = atof(value);
        DEBUG_VAR(water_temperature);
        break;
    case 'm':
        moisture_level = atof(value);
        DEBUG_VAR(moisture_level);
        break;
    }
}

/* Parse command from serial */
void parse_command_from_serial()
{
    while (Serial.available()) {
        char command = Serial.read();
        int index;
        char value[10] = "\0";
        for (index = 0; Serial.available(); index++) {
            value[index] = Serial.read();
            if (value[index] == '\n' || value[index] == ',') {
                value[index] = '\0';
                break; // command termination character
            }
        }
        apply_command(command, value);
    }
}

/* Setup */
void setup()
{
    Serial.begin(9600);
    Serial.println("setup()");

    delay(5000);

    /* LCD display */
    lcd.begin(16, 2); // LCD's number of columns and rows

    /* TH02 sensor */
    TH02.begin();

    /* Servo */
    myservo.attach(pinServo);
    myservo.write(90); // Servo in the middle

    /* Relay */
    pinMode(pinRelay, OUTPUT);
    digitalWrite(pinRelay, LOW); // OFF

    /* Button */
    pinMode(buttonPin, INPUT_PULLUP);
    attachInterrupt(buttonPin, localButtonPressed, CHANGE); // Press and release

    /* Initialization */
    status_state[0] = OFF;
    status_state[1] = '\0';
    IoT_Error_t rc = FAILURE;
    char JsonDocumentBuffer[MAX_LENGTH_OF_UPDATE_JSON_BUFFER];
    size_t sizeOfJsonDocumentBuffer = sizeof(JsonDocumentBuffer) / sizeof(JsonDocumentBuffer[0]);

    /* remote_button json */
    remoteButton.cb = remoteButton_Callback;
    remoteButton.pData = &remote_button;
    remoteButton.pKey = "R";
    remoteButton.type = SHADOW_JSON_BOOL;

    /* gas_cost json */
    gasCost.cb = gasCost_Callback;
    gasCost.pKey = "G";
    gasCost.pData = &gas_cost;
    gasCost.type = SHADOW_JSON_FLOAT;

    /* electric_cost json */
    electricCost.cb = electricCost_Callback;
    electricCost.pKey = "E";
    electricCost.pData = &electric_cost;
    electricCost.type = SHADOW_JSON_FLOAT;

    /* target_temperature json */
    targetTemperature.cb = targetTemperature_Callback;
    targetTemperature.pKey = "T";
    targetTemperature.pData = &target_temperature;
    targetTemperature.type = SHADOW_JSON_FLOAT;

    /* water_temperature */
    waterTemperature.cb = NULL;
    waterTemperature.pKey = "W";
    waterTemperature.pData = &water_temperature;
    waterTemperature.type = SHADOW_JSON_FLOAT;

    /* status_state json */
    statusState.cb = NULL;
    statusState.pKey = "S";
    statusState.pData = &status_state;
    statusState.type = SHADOW_JSON_STRING;

    /* Debug desired json */
    jsonStruct_t deltaObject;
    deltaObject.pData = NULL;
    deltaObject.pKey = "state";
    deltaObject.type = SHADOW_JSON_OBJECT;
    deltaObject.cb = DeltaCallback;

    char rootCA[PATH_MAX + 1];
    char clientCRT[PATH_MAX + 1];
    char clientKey[PATH_MAX + 1];
    char CurrentWD[PATH_MAX + 1];

    IOT_INFO("\nAWS IoT SDK Version %d.%d.%d-%s\n", VERSION_MAJOR, VERSION_MINOR,
        VERSION_PATCH, VERSION_TAG);

    getcwd(CurrentWD, sizeof(CurrentWD));
    snprintf(rootCA, PATH_MAX + 1, "%s/%s/%s", CurrentWD, certDirectory,
        AWS_IOT_ROOT_CA_FILENAME);
    snprintf(clientCRT, PATH_MAX + 1, "%s/%s/%s", CurrentWD, certDirectory,
        AWS_IOT_CERTIFICATE_FILENAME);
    snprintf(clientKey, PATH_MAX + 1, "%s/%s/%s", CurrentWD, certDirectory,
        AWS_IOT_PRIVATE_KEY_FILENAME);

    IOT_DEBUG("rootCA %s", rootCA);
    IOT_DEBUG("clientCRT %s", clientCRT);
    IOT_DEBUG("clientKey %s", clientKey);

    Serial.print("rootCA: ");
    Serial.println(rootCA);
    Serial.print("clientCRT: ");
    Serial.println(clientCRT);
    Serial.print("clientKey: ");
    Serial.println(clientKey);

    ShadowInitParameters_t sp = ShadowInitParametersDefault;
    sp.pHost = AWS_IOT_MQTT_HOST;
    sp.port = AWS_IOT_MQTT_PORT;
    sp.pClientCRT = clientCRT;
    sp.pClientKey = clientKey;
    sp.pRootCA = rootCA;
    sp.enableAutoReconnect = false;
    sp.disconnectHandler = NULL;

    /* Shadow Init */
    rc = aws_iot_shadow_init(&mqttClient, &sp);
    if (SUCCESS != rc) {
        IOT_ERROR("Shadow Init Error");
        Serial.println("Shadow Init Error");
        return;
    }
    else {
        IOT_INFO("Shadow Init");
        Serial.println("Shadow Init");
    }

    ShadowConnectParameters_t scp = ShadowConnectParametersDefault;
    scp.pMyThingName = AWS_IOT_MY_THING_NAME;
    scp.pMqttClientId = AWS_IOT_MQTT_CLIENT_ID;
    scp.mqttClientIdLen = (uint16_t)strlen(AWS_IOT_MQTT_CLIENT_ID);

    /* Shadow Connection */
    rc = aws_iot_shadow_connect(&mqttClient, &scp);
    if (SUCCESS != rc) {
        IOT_ERROR("Shadow Connection Error");
        Serial.println("Shadow Connection Error");
        return;
    }
    else {
        IOT_INFO("Shadow Connect");
        Serial.println("Shadow Connect");
    }

    /* Auto Reconnect to true */
    rc = aws_iot_shadow_set_autoreconnect_status(&mqttClient, true);
    if (SUCCESS != rc) {
        IOT_ERROR("Unable to set Auto Reconnect to true - %d", rc);
        Serial.println("Unable to set Auto Reconnect to true");
        return;
    }
    else {
        Serial.println("aws_iot_shadow_set_autoreconnect_status OK");
    }

    /* yield */
    rc = aws_iot_shadow_yield(&mqttClient, 200); // 200 ms
    if (SUCCESS != rc) {
        IOT_ERROR("aws_iot_shadow_yield error");
        Serial.println("aws_iot_shadow_yield error");
    }
    else {
        Serial.println("aws_iot_shadow_yield OK");
    }

    /* Register remote_button desired */
    rc = aws_iot_shadow_register_delta(&mqttClient, &remoteButton);
    if (SUCCESS != rc) {
        IOT_ERROR("Shadow Register Delta Error - remoteButton");
        Serial.println("Shadow Register Delta Error - remoteButton");
    }
    else {
        Serial.println("aws_iot_shadow_register_delta remoteButton OK");
    }

    /* Register gas_cost desired */
    rc = aws_iot_shadow_register_delta(&mqttClient, &gasCost);
    if (SUCCESS != rc) {
        IOT_ERROR("Shadow Register Delta Error - gasCost");
        Serial.println("Shadow Register Delta Error - gasCost");
    }
    else {
        Serial.println("aws_iot_shadow_register_delta gasCost OK");
    }

    /* Register electric_cost desired */
    rc = aws_iot_shadow_register_delta(&mqttClient, &electricCost);
    if (SUCCESS != rc) {
        IOT_ERROR("Shadow Register Delta Error - electricCost");
        Serial.println("Shadow Register Delta Error - electricCost");
    }
    else {
        Serial.println("aws_iot_shadow_register_delta electricCost OK");
    }

    /* Register target_temperature desired */
    rc = aws_iot_shadow_register_delta(&mqttClient, &targetTemperature);
    if (SUCCESS != rc) {
        IOT_ERROR("Shadow Register Delta Error - targetTemperature");
        Serial.println("Shadow Register Delta Error - targetTemperature");
    }
    else {
        Serial.println("aws_iot_shadow_register_delta targetTemperature OK");
    }

    /* http://aws-iot-device-sdk-embedded-c-docs.s3-website-us-east-1.amazonaws.com/aws__iot__shadow__interface_8h.html */
    rc = aws_iot_shadow_register_delta(&mqttClient, &deltaObject);
    if (SUCCESS != rc) {
        IOT_ERROR("Shadow Register Delta Error - deltaObject");
        Serial.println("Shadow Register Delta Error - deltaObject");
    }
    else {
        Serial.println("aws_iot_shadow_register_delta deltaObject OK");
    }

    /* GET */
    rc = aws_iot_shadow_get(&mqttClient, AWS_IOT_MY_THING_NAME, shadowCallback, NULL, 800, true);
    if (SUCCESS != rc) {
        IOT_ERROR("aws_iot_shadow_get error");
        Serial.println("aws_iot_shadow_get error");
    }
    else {
        Serial.println("aws_iot_shadow_get OK");
    }

    Serial.println("setup OK");

    // loop and publish
    while (NETWORK_ATTEMPTING_RECONNECT == rc || NETWORK_RECONNECTED == rc || SUCCESS == rc) {
        delay(100);

        parse_command_from_serial();
        moisture_level = analogRead(pinMoisture);
        efficiency = fabs((gas_cost) * (1.0 - ((moisture_level) / 1000.0) * 8));

        if (TH02.isAvailable()) {
            water_temperature = roundf((TH02.ReadTemperature()) * 100) / 100;        
        } else {
            water_temperature = thermistor.read() / 10;            
        }
        printLCD();

        if (local_button || remote_button) { // OR logic, activated if one of them
            status_state[0] = stateMachine();
            setServo();
            relayHandle();
        }

        /* https://www.arduino.cc/en/Tutorial/BlinkWithoutDelay */
        currentMillis = millis();
        if (currentMillis - previousMillis < refreshRate) {
            continue;
        }
        else {
            previousMillis = currentMillis;
        }

        IOT_INFO("\n===========================================================\n");
        Serial.println("=========================================================");

        rc = aws_iot_shadow_yield(&mqttClient, 200); // 200 ms
        if (NETWORK_ATTEMPTING_RECONNECT == rc) {
            Serial.println("NETWORK_ATTEMPTING_RECONNECT");
            delay(1000);
            // If the client is attempting to reconnect we will skip the rest of the
            // loop.
            continue;
        }

        /* open json document */
        rc = aws_iot_shadow_init_json_document(JsonDocumentBuffer,
            sizeOfJsonDocumentBuffer);
        if (SUCCESS == rc) {
            rc = aws_iot_shadow_add_reported( // add 3 reporteds
                JsonDocumentBuffer, sizeOfJsonDocumentBuffer, 3, &waterTemperature,
                &statusState, &remoteButton);
            if (SUCCESS == rc) {
                rc = aws_iot_finalize_json_document(JsonDocumentBuffer,
                    sizeOfJsonDocumentBuffer); // close json document
                if (SUCCESS == rc) {
                    IOT_INFO("Update Shadow: %s", JsonDocumentBuffer);
                    Serial.print("Update Shadow: ");
                    Serial.println(JsonDocumentBuffer);
                    rc = aws_iot_shadow_update(&mqttClient, AWS_IOT_MY_THING_NAME,
                        JsonDocumentBuffer,
                        ShadowUpdateStatusCallback, NULL, 4, true); // 4 seconds
                }
            }
        }
        IOT_INFO("*************************************************************\n");
        Serial.println("*********************************************************");
    }

    if (SUCCESS != rc) {
        IOT_ERROR("An error occurred in the loop %d", rc);
        Serial.println("An error occurred in the loop");
    }
    else {
        IOT_INFO("Disconnecting");
        Serial.println("Disconnecting");
    }

    rc = aws_iot_shadow_disconnect(&mqttClient);
    if (SUCCESS != rc) {
        IOT_ERROR("Disconnect error %d", rc);
        Serial.println("Disconnect error");
    }
    else {
        Serial.println("Disconnecting OK");
    }
}

void loop()
{
    /* Empty, because there is a while() in the setup() */
}
